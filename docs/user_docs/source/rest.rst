.. _rest_interface:

=============
Using the API
=============


API structure
=============
The root of the GraceDB API is at `<https://gracedb.ligo.org/api/>`__.
There are two main components of the API:

- `Events <https://gracedb.ligo.org/api/events/>`__
- `Superevents <https://gracedb.ligo.org/api/superevents/>`__

These URLs provide a list of all events and superevents which are accessible to you.

Information about an individual event or superevent can be accessed by appending its graceid or superevent id to those links.
Examples:

- ``/api/events/G1234/``
- ``/api/superevents/S140309abc/``

Annotations to events and superevents are similarly nested:

- ``/api/superevents/S140309abc/logs/`` (list of logs for S140309abc)
- ``/api/superevents/S140309abc/logs/1/`` (log #1 for S140309abc)
- ``/api/superevents/S140309abc/voevents/`` (list of VOEvents for S140309abc)
- ``/api/superevents/S140309abc/files/file1.xml`` (``file.xml``, which is associated with S140309abc)
- and so on.


Web-browsable API
=================
If you visit any of the above links in a web browser, a human-readable version of the API is rendered.
There is a ``links`` key at the API root which links to the main events and superevents API resources.
There are also ``links`` keys in the superevent and event data models, so much of the web API can be traversed using links.
The web-browsable API can be useful to quickly view an object's representation (superevent, event, log message, etc.) as returned by the API, for use in programmatic queries to the API.
Otherwise, a description of the API representation for various objects is available in :ref:`models`.


Using the API programmatically
==============================
At present, the GraceDB API is not well-documented, and it may be challenging to write a custom client for accessing it.
The LSC maintains a Python package, ligo-gracedb, for simplifying access to the API.
This package's `documentation <https://gw.readthedocs.io/ligo-gracedb>`__ is comprehensive and includes basic installation, configuration, and usage.
For non-Python users, ligo-gracedb also comes with a command-line client.


Authenticating to the API
=========================
As with the GraceDB web interface, unauthenticated access is available to the API, but is restricted to data which have been deemed suitable for public release.

If you are viewing the API through a web browser, then any authentication credentials you previously provided will continue to allow access.
For programmatic access to the API, LIGO/Virgo users can use X.509 certificates generated by ``ligo-proxy-init`` or robot certificates obtained from the LIGO authentication team.
More information on how to use these credentials with ligo-gracedb is available in its `documentation <https://gw.readthedocs.io/projects/ligo-gracedb/en/latest/user_guide.html#credentials>`__.

Finally, more details on authentication and authorization in GraceDB are provided in :ref:`auth`.


Coping with request rate limits
=====================================
GraceDB limits any individual user to no more than 10 event creation requests or 10 annotation requests per second.
This is to avoid situations where an automated data analysis pipeline malfunctions and sends huge rates of requests to GraceDB, crashing the server.
If a user's request is *throttled* in this way, the server returns a response with HTTP status ``429`` and reason ``TOO MANY REQUESTS``.
The recommended wait time is also included in the message, and is available through the ``retry-after`` when using the ligo-gracedb client.
If you have reason to believe that your request may be throttled, you can wrap it in a ``while`` loop in the following way::

    from ligo.gracedb.rest import GraceDb, HTTPError
    import time
    import json

    gracedb = GraceDb()
    graceid = 'T123456'

    success = False
    while not success:
        try:
            r = gracedb.writeLog(graceid, "Hello, this is a log message.")
            success = True
        except HTTPError, e:
            try:
                rdict = json.loads(e.message)
                if 'retry-after' in rdict.keys():
                    time.sleep(int(rdict['retry-after']))
                    continue
                else:
                    break
            except:
                break
