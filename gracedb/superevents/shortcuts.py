from .models import Superevent

def is_superevent(obj):
    return isinstance(obj, Superevent)
